package com.arifariyan.loginmvp;

/**
 * Created by arifariyan on 11/18/16.
 */

public interface MainContract {
    interface View {
        void loginSuccess(String title);
        void logout(String title);
        void showMessage(String message);
    }

    interface Presenter {
        void doLogin(String username, String password);
    }

}
